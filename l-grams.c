#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <stdbool.h>
#include <string.h>
#include "common.h"

char buffer[BUF_SIZE];
char excluded[BUF_SIZE];

int main(int argc, char **argv)
{
	char *filename = basename(argv[0]);
	int l = strtol(filename, NULL, 10);
	if (argc != 1 || l == 0) {
		printf("usage: ./{1,2,3}-grams\n");
		exit(1);
	}

	fgets(buffer, BUF_SIZE, stdin);
	int length = strlen(buffer);

	for (int i = 0; i < length; ++i) {
		eallowed(buffer[i]);
		if (excluded[i]) continue;
		int count = 0;
		int prev_j = i;
		for (int j = i; j < length; ++j) {
			bool match = true;
			for (int k = 0; k < l; ++k) {
				if (buffer[i+k] != buffer[j+k]) { match = false; break; }
			}
			if (match) {
				++count;
				excluded[j] = 1;
				if (count > 1) {
					int distance = j - prev_j;
					printf("%d ", distance);
					prev_j += distance;
				}
			}
		}
		if (count > 1)
			printf("\n");
	}
}
