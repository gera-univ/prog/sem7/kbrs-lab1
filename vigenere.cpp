#include <bits/stdc++.h>
#include "common.h"

using namespace std;

#ifdef ENCRYPT
char symadd(char x, char y) { return 'a' + ((x-'a')+(y-'a'))%26; }
#else
char symsub(char x, char y) { return 'a' + ((x-'a')-(y-'a')+26)%26; }
#endif

string encrypt(string str, string key)
{
	string cipher_text;

	int j = 0;
	for (int i = 0; i < str.length(); i++)
	{
		eallowed(str[i]);
		eallowed(key[j]);
#ifdef ENCRYPT
		char x = symadd(str[i], key[j]);
#else
		char x = symsub(str[i], key[j]);
#endif
		j = (j + 1) % key.length();
		cipher_text.push_back(x);
	}
	return cipher_text;
}

int main(int argc, char **argv)
{
	if (argc < 2) {printf("usage: %s KEY\n", argv[0]); exit(1);}
	string key(argv[1]);
	string str;

	while (getline(cin, str)) {
		string cipher_text = encrypt(str, key);
		cout << cipher_text;
	}
	return 0;
}
