CFLAGS=-g
CXXFLAGS=-g

L-GRAMS = 1-grams 2-grams 3-grams 4-grams
BIN = filter $(L-GRAMS) encrypt decrypt gcd_stats
all: $(BIN)

0-grams: l-grams.c
	$(CC) $^ -o $@ $(CFLAGS) -DBUF_SIZE=1048576

$(L-GRAMS): 0-grams
	ln -f 0-grams $@

encrypt: vigenere.cpp
	$(CXX) $^ $(CXXFLAGS) -o $@ -DENCRYPT

decrypt: vigenere.cpp
	$(CXX) $^ $(CXXFLAGS) -o $@

filter: filter.c
	$(CC) $^ -o $@ $(CFLAGS)

gcd_stats: gcd_stats.cpp
	$(CXX) $^ $(CXXFLAGS) -o $@ -DSAMPLE_SIZE=5 -DSAMPLE_COUNT=1000

.PHONY: clean
clean:
	-rm 0-grams $(BIN)

