#include <stdio.h>
#include <ctype.h>

int main()
{
	for (;;) {
		char c = fgetc(stdin);
		if (c == EOF) return 0;
		if (c >= 'a' && c <= 'z')
			putc(c, stdout);
		else if (c >= 'A' && c <= 'Z')
			putc(tolower(c), stdout);
	}
}
