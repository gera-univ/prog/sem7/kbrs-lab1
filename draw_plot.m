#!/usr/bin/env octave
password_full = "abcdefghji";

data = []
for l = 1:4
l_data = []
for password_length = 1:10
  password = password_full(1:password_length);
  command = sprintf("./encrypt <alice.txt %s | ./%d-grams | ./gcd_stats",
                    password, l);
  [~, text] = system (command);

  text_columns = textscan(text, '%d %d');
  gcds = text_columns{1,1}';
  counts = text_columns{1,2}';

  i = find(gcds==password_length);
  hits = counts(i)

  if (isempty(hits))
    hits = [0]
  endif
  l_data = [l_data hits]
endfor
data=[data
l_data]
endfor

% data = [
%    961   104    20     6     1     0     0     1     0     0
%    966   378   240   134    80    47    40    21    15     8
%    962   716   621   526   433   341   284   255   209   185
%    967   907   881   908   839   825   778   717   732   687
% ]
bar(1:10, data')
legend('1-grams','2-grams','3-grams','4-grams')
xlabel("Password length")
ylabel("GCD matches password length per 1000")
pause
