A set of tools for finding Vigenere key length using Kasiski analysis.

# Usage demonstration

1. Compile everything

```
$ make
```

2. Acquire some text

```
$ curl https://www.gutenberg.org/cache/epub/11/pg11.txt >alice_in_wonderland.txt
```

3. Filter it to remove characters not in the Latin alphabet

```
$ ./filter <alice_in_wonderland.txt >alice.txt
```

4. Encrypt it with a password (must only use 'a'-'z')

```
$ ./encrypt eatme <alice.txt >alice_enc.txt
```

5. Compute distances between 1,2,3,4-grams

```
$ ./1-grams <alice_enc.txt >alice_1g.txt
$ ./2-grams <alice_enc.txt >alice_2g.txt
$ ./3-grams <alice_enc.txt >alice_3g.txt
$ ./4-grams <alice_enc.txt >alice_4g.txt
```

6. Run `gcd_stats` on the distance lists. This program takes random samples of length 5 from the set of distances and computes their GCDs. It does that 1000 times and counts the number of times each result is obtained.

```
$ ./gcd_stats <alice_1g.txt
1	964
2	29
3	6
5	1
$ ./gcd_stats <alice_2g.txt
1	894
2	29
3	3
4	1
5	69
10	3
15	1
$ ./gcd_stats <alice_3g.txt
1	560
2	10
3	4
4	1
5	412
10	12
20	1
$ ./gcd_stats <alice_4g.txt
1	173
2	5
3	1
5	793
6	1
10	26
15	1
```

The longer l-grams we use, the more likely is the GCD to match our password length. As you can see in the last example, the length of our password (`eatme`) has the most hits.

![](plot.svg)
