#include <bits/stdc++.h>

using namespace std;

int get_sample_gcd(int size, vector<int> const &numbers)
{
	int result = numbers[rand() % numbers.size()];
	for (int i = 0; i < size - 1; ++i)
	{
		result = gcd(result, numbers[rand() % numbers.size()]);
	}
	return result;
}

int main()
{
	srand(time(0));

	vector<int> numbers;
	int n, result;
	while (cin >> n) {
		numbers.emplace_back(n);
	}

	if (numbers.size() < SAMPLE_SIZE * SAMPLE_COUNT) {
		fprintf(stderr, "feed this program at least %d numbers\n", SAMPLE_SIZE * SAMPLE_COUNT);
		exit(1);
	}

	map<int, int> counts;
	for (int i = 0; i < SAMPLE_COUNT; ++i)
	{
		int sample_gcd = get_sample_gcd(SAMPLE_SIZE, numbers);
		counts[sample_gcd] += 1;
	}

	for (auto p = counts.begin(); p != counts.end(); ++p) {
		cout << p->first << "\t" << p->second << "\n";
	}
}
